# Ejercicio Chat en Java utilizando Sockets

## Sockets

En la actualidad, muchos de los procesos que se ejecutan en una ordenador requieren obtener o enviar información a otros procesos que se localizan en un ordenador diferente.  Para lograr esta comunicación  se  utilizan los protocolos de comunicación __TCP__ y __UDP__.

El protocolo __TCP__ (_Transmission Control Protocol_) establece un conducto de comunicación punto a punto entre dos ordenadores, es decir, cuando se requiere la transmisión de un flujo de datos entre dos equipos, el protocolo TCP establece un conducto exclusivo entre dichos equipos por el cual los datos serán transmitidos y este perdurará hasta que la transmisión haya finalizado, gracias a esto _TCP_ garantiza que los datos enviados de un extremo de la conexión lleguen al otro extremo y en el mismo orden en que fueron enviados. Las características que posee TCP hacen que el protocolo sea conocido como un protocolo orientado a conexión.

Mientras tanto también existe un protocolo no orientado a la conexión llamado __UDP__. El protocolo _UDP_ no es orientado a la conexión debido a que la forma de trasmitir los datos no garantiza en primera instancia su llegada al destino, e incluso si este llegara al destino final, tampoco garantiza la integridad de los datos. UPD hace la transmisión de los datos sin establecer un conducto de comunicación exclusiva como lo hace TCP, además utiliza _datagramas_, los cuales contienen una porción de la información y que son enviados a la red en espera de ser capturados por el equipo destino. Cuando el destino captura los datagramas debe reconstruir la información, para esto debe ordenar la información que recibe ya que la información transmitida no viene con un orden específico, además se  debe tener conciencia de que no toda la información va a llegar.

Socket es un concepto abstracto por el cual dos programas situados en dos ordenadores distintos pueden intercambiar cualquier flujo de datos de manera fiable y ordenada, en internet constituye el mecanismo para la entrega de paquetes de datos provenientes de un ordenador, para que dos programas puedan comunicarse entre si es necesario ciertos requisitos.

Cuando un servidor se ejecuta sobre un ordenador tiene un socket que responde a un puerto, este lo único que hace esperar a través de un socket a que un cliente haga una petición.

El cliente conoce el nombre del servidor y el número del puerto en el cual el servidor está conectado, para realizar una petición de conexión el cliente intenta encontrar al servidor.


## Clase Servidor

La clase _Servidor_ hace uso de las siguientes clases:

* __Socket__: Para recibir y enviar flujos de datos por la red
* __ServerSocket__: Para aceptar las conexiones del cliente.
* __DataInputStream__: Flujo de entrada para procesar los datos enviados por el cliente
* __DataOutputStream__: Flujo de salida que procesa los datos que se enviarán al cliente.
* __Scanner__: Para leer datos desde teclado.
* __Booleano__: Para manipular el hilo.

### Método Conexión

Lo primero que tenemos que hacer es poner al __ServerSocket__ a escuchar para poder aceptar peticiones del cliente, para ello vamos a crear un método pasándole como parámetro un numero el cual será el número del puerto.

Usamos el constructor __Serversocket(numeroPuerto)__ el cual se le debe pasar como parámetros un numero de puerto donde se pondrá a escuchar recibir peticiones de los clientes y llamamos a su método __accept()__ el cual cuando se conecte un cliente aceptara la petición de dicho cliente, hay que tener en cuenta que estas clases lanzan excepción por lo que hay capturarlo entre un try y un catch, cerramos conexión.

Con el __Serversocket__ a la escucha e el puerto indicado tenemos que abrir los flujos de datos para procesar la información que recibimos y enviamos para ello creamos dos método uno que enviara datos y otro que recibirá.

```java
public void conexion(int numeroPuerto) {
    try {
        socketServicio = new ServerSocket(numeroPuerto);
        System.out.println("El servidor se esta escuchando en el puerto: " + numeroPuerto);
        miServicio = socketServicio.accept();

        ...

    } catch (Exception ex) {
        System.out.println("Error al abrir los sockets");
    }
}
```

### Método Enviar Datos

Se le pasara como parámetros un _String_ el cual usara para recibir cadenas de texto para enviárselos al cliente, instanciamos la clase __DataOutputStream__ el cual le pasaremos como parámetro un __OutputStream__ el cual se usara con método llamado desde el socket __misServicio__, escribimos en el con el método __writeUTF(datos)__ de la clase __DataOutputStream__ el cual le pasaremos como parámetro el _String_ del método enviarDatos, limpiamos con el método __flush()__ y capturamos su excepción.

```java
public void conexion(int numeroPuerto) {
    try {
        socketServicio = new ServerSocket(numeroPuerto);
        System.out.println("El servidor se esta escuchando en el puerto: " + numeroPuerto);
        miServicio = socketServicio.accept();

        ...

    } catch (Exception ex) {
        System.out.println("Error al abrir los sockets");
    }
}
```

### Método Recibir Datos

Es un método el cual mediante la clase __DataInputStream__ transmitirá los datos del servidor al cliente abres un flujo recibes los datos y los muestras por pantalla.

```java
public void recibirDatos() {
    try {
        inputStream = miServicio.getInputStream();
        entradaDatos = new DataInputStream(inputStream);
        System.out.println(entradaDatos.readUTF());
    } catch (IOException ex) {
        Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
    }
}
```

### Método Cerrar Todo

Este método cierra todas las conexiones abiertas por el _SocketServer_ y el _Socket_ con el método __close()__.

```java
public void cerrarTodo() {
    try {
        salidaDatos.close();
        entradaDatos.close();
        socketServicio.close();
        miServicio.close();

    } catch(IOException ex) {
        Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
    }
}
```

### Método Thread

Para que esto funcione tendremos que crear en el método __conexión__ un hilo que se encargue de recibir la información que le envié el Cliente usando el método __recibirDatos__ de la clase Servidor y iniciando el hilo que ejecutara cada vez que lleguen datos del cliente.

```java
Thread hilo = new Thread(new Runnable() {
@Override
  public void run() {
     while (opcion) {
        System.out.print("SERVIDOR: ");
        recibirDatos();
     }
  }
});
hilo.start();
```

### Scanner

En esta parte del código escribiremos por consola usando la clase Scanner para enviar la información usando el método __enviarDatos()__ todo esto lo encerramos en un __while()__ para poder repetir este proceso hasta que decidamos poner fin a la conversación, en el usaremos un _boobleano_ para controlar su estado.

```java
while (opcion){
    scanner = new Scanner(System.in);
    esctribir = scanner.nextLine();
    if (!esctribir.equals("CLIENTE: fin")) {
        enviarDatos("SERVIDOR: " + esctribir);
    } else {
        opcion = false;
    }
}
```

### Método main

Y para finalizar en el __main__ llamamos a los métodos de nuestra clase para hacer funcionar al servidor.

```java
public static void main(String[] args) {
    Servidor serv = new Servidor();
    serv.conexion(5555);
}
```

### Código de la Clase Servidor

```java
// Fichero: Servidor.java
package com.psp.jonaygarcia.chatserver;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.psp.jonaygarcia.chatclient.*;

public class Servidor {

    private Socket miServicio;
    private ServerSocket socketServicio;

    private OutputStream outputStream;
    private InputStream inputStream;

    private DataOutputStream salidaDatos;
    private DataInputStream entradaDatos;

    private boolean opcion = true;
    private Scanner scanner;
    private String esctribir;

    //APERTURA DE SOCKET
    public void conexion(int numeroPuerto) {
        try {
            socketServicio = new ServerSocket(numeroPuerto);
            System.out.println("El servidor se esta escuchando en el puerto: " + numeroPuerto);
            miServicio = socketServicio.accept();
            Thread hilo = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (opcion) {
                        System.out.print("SERVIDOR: ");
                        recibirDatos();

                    }
                }
            });
            hilo.start();
            while (opcion) {
                scanner = new Scanner(System.in);
                esctribir = scanner.nextLine();
                if (!esctribir.equals("CLIENTE: fin")) {
                    enviarDatos("SERVIDOR: " + esctribir);
                } else {
                    opcion = false;
                }
            }
            miServicio.close();
        } catch (Exception ex) {
            System.out.println("Error al abrir los sockets");
        }
    }

    public void enviarDatos(String datos) {
        try {
            outputStream = miServicio.getOutputStream();
            salidaDatos = new DataOutputStream(outputStream);
            salidaDatos.writeUTF(datos);
            salidaDatos.flush();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void recibirDatos() {
        try {
            inputStream = miServicio.getInputStream();
            entradaDatos = new DataInputStream(inputStream);
            System.out.println(entradaDatos.readUTF());
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cerrarTodo() {
        try {
            salidaDatos.close();
            entradaDatos.close();
            socketServicio.close();
            miServicio.close();

        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        Servidor serv = new Servidor();
        serv.conexion(5555);
    }
}
```

## Clase Cliente

La clase Cliente es análoga a la clase Servidor salvo que carece de __SeverSocket__ y toda la funcionalidad que ello conlleva y que en el método _conexión_ le pasaremos como parámetro el número del puerto y la ip de la maquina a la que se quiera conectar, usando el constructor __Socket(numeroIP, numeroPuerto)__, todo lo demás es igual al Servidor.

```java
// Fichero: Cliente.java
package com.psp.jonaygarcia.chatclient;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.psp.jonaygarcia.chatserver.*;

public class Cliente {
    private Socket socketCliente;

    private InputStream inputStream;
    private OutputStream outputStream;

    private DataInputStream entradaDatos;
    private DataOutputStream SalidaDatos;

    private boolean opcion = true;

    private Scanner scanner;
    private String esctribir;
    public void conexion(int numeroPuerto, String ipMaquina) {
        try {
            socketCliente = new Socket(ipMaquina, numeroPuerto);
            Thread hilo1 = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (opcion) {
                        escucharDatos(socketCliente);
                        System.out.print("CLIENTE: ");
                    }
                }
            });
            hilo1.start();
            while (opcion) {
                scanner = new Scanner(System.in);
                esctribir = scanner.nextLine();
                if (!esctribir.equals("SERVIDOR: fin")) {
                    enviarDatos("CLIENTE: " + esctribir);
                } else {
                    opcion = false;
                    cerrarTodo();
                }
            }

        } catch (Exception ex) {
            System.out.println("ERROR AL ABRIR LOS SOCKETS CLIENTE " + ex.getMessage());
        }
    }
    public void escucharDatos(Socket socket) {
        try {
            inputStream = socket.getInputStream();
            entradaDatos = new DataInputStream(inputStream);
            System.out.println(entradaDatos.readUTF());
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void enviarDatos(String datos) {
        try {
            outputStream = socketCliente.getOutputStream();
            SalidaDatos = new DataOutputStream(outputStream);
            SalidaDatos.writeUTF(datos);
            SalidaDatos.flush();
        } catch (IOException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void cerrarTodo() {
        try {
            SalidaDatos.close();
            entradaDatos.close();
            socketCliente.close();
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void main(String[] args) {
        Cliente cli = new Cliente();
        cli.conexion(5555, "localhost");
    }
}
```
